package com.example.cachelab;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import jakarta.annotation.PostConstruct;

@Controller
public class SimpleController {

        private SimpleService simpleService;

        public SimpleController(SimpleService simpleService) {
                this.simpleService = simpleService;
        }

        @GetMapping("/naive/{id}")
        public ResponseEntity<String> naiveCache(@PathVariable Long id) {

                String truc = this.simpleService.naiveCache(id);

                return ResponseEntity
                                .ok()
                                .cacheControl(CacheControl.noCache())
                                .body(truc);
        }

        @GetMapping("/full/{id}")
        public ResponseEntity<String> fullCache(@PathVariable Long id) {

                String truc = this.simpleService.getById(id);

                return ResponseEntity
                                .ok()
                                .cacheControl(
                                                CacheControl.maxAge(10, TimeUnit.DAYS)
                                                                .immutable()
                                                                .noTransform()
                                                                .cachePrivate())
                                .body(truc);
        }

        @GetMapping("/network/{id}")
        public ResponseEntity<String> networkCache(@PathVariable Long id) {

                String truc = this.simpleService.getById(id);
                return ResponseEntity
                                .ok()
                                .cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS))
                                .eTag(id.toString()) // version of object is id : never change for this id
                                .body(truc);
        }

        @GetMapping("/app/{id}")
        public ResponseEntity<String> applicationCache(@PathVariable Long id) {

                String truc = this.simpleService.getByIdCachable(id);
                return ResponseEntity
                                .ok()
                                .cacheControl(CacheControl.noCache())
                                .body(truc);
        }

        @GetMapping("/no/{id}")
        public ResponseEntity<String> noCache(@PathVariable Long id) {

                String truc = this.simpleService.getById(id);
                return ResponseEntity
                                .ok()
                                .cacheControl(CacheControl.noCache())
                                .body(truc);
        }

        // methode naive d'usage de cache Cafeine
        private Cache<Long, String> cache;

        @PostConstruct
        public void post() {
                cache = Caffeine.newBuilder()
                                .maximumSize(10_000)
                                .expireAfterWrite(Duration.ofMinutes(5))
                                .refreshAfterWrite(Duration.ofMinutes(1))
                                .build(key -> this.simpleService.getById(key));
        }

        @GetMapping("/double/{id}")
        public ResponseEntity<String> doubleCache(@PathVariable Long id)
                        throws InterruptedException, ExecutionException {

                CompletableFuture<String> completableFuture1 = CompletableFuture
                                .supplyAsync(() -> this.cache.get(id, (key) -> this.simpleService.getById(key)));

                CompletableFuture<String> completableFuture2 = CompletableFuture
                                .supplyAsync(() -> this.cache.get(id, (key) -> this.simpleService.getById(key)));

                while (!completableFuture1.isDone() || !completableFuture2.isDone()) {
                }
                String truc = completableFuture1.get();
                String truc2 = completableFuture2.get();
                return ResponseEntity
                                .ok()
                                .cacheControl(CacheControl.noCache())
                                .body(truc + " ==> " + truc2);
        }
}
