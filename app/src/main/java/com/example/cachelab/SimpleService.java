package com.example.cachelab;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class SimpleService {
  private Map<Long, String> cache = new HashMap<>();

  public String naiveCache(Long id) {
    if (cache.containsKey(id)) {
      return cache.get(id);
    } else {
      String value = this.getById(id);
      cache.put(id, value);
      return value;
    }
  }

  @Cacheable(value = "get_id")
  public String getByIdCachable(long id) {
    return this.getById(id);
  }

  public String getById(long id) {
    System.out.println(
        "Service SimpleService.getById appellé. NE FAITE PAS DES LOGS AVEC  System.out !!!! NON VRAIMENT!!! Je suis sérieux !!!");
    simulateSlowService();

    return id + "_" + LocalDateTime.now();
  }

  // Don't do this at home
  private void simulateSlowService() {
    try {
      long time = 3000L;
      Thread.sleep(time);
    } catch (InterruptedException e) {
      throw new IllegalStateException(e);
    }
  }

}